package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerViewInterface {

    RecyclerView rv1;
    public static String TAG = "RV1";

    public static ArrayList<Mahasiswa> data;

    EditText inputNIM, inputNama;
    Button bt1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        data = getData();
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data, this);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));


//        add
        inputNIM = findViewById(R.id.inputNIM);
        inputNama = findViewById(R.id.inputNama);
        bt1 = findViewById(R.id.bt1);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inpNama = inputNama.getText().toString();
                String inpNIM = inputNIM.getText().toString();
                if(inpNama == null || inpNIM == null || inpNama.length() == 0 || inpNIM.length() == 0){

                } else {
                    inputNama.setText("");
                    inputNIM.setText("");

                    addData(inpNIM, inpNama);
                    rv1.setAdapter(adapter);
                }

            }
        });
    }

    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }

    public static void addData(String NIM, String Nama){
        Mahasiswa mhsNew = new Mahasiswa();
        mhsNew.nim = NIM;
        mhsNew.nama = Nama;
        data.add(mhsNew);
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(MainActivity.this, Activity2.class);

        intent.putExtra("NIM", data.get(position).nim);
        intent.putExtra("Nama", data.get(position).nama);

        startActivity(intent);
    }
}